#!/usr/bin/env python

from __future__ import print_function
import sys
from itertools import combinations, product, izip
import subprocess as sp
import pygraph as pg
from pygraph.algorithms.generators import generate
from pygraph.readwrite.dot import write


def find_all_paths(adj, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    if not adj.has_key(start):
        return []
    paths = []
    for node in adj[start]:
        if node not in path:
            newpaths = find_all_paths(adj, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths

def build_adjacency(gr):
    adj = {}
    for node in gr.nodes():
        adj[node] = gr.neighbors(node)
    return adj

def cut_helper(num):
    r = set(xrange(1,num-1))
    for i in xrange(len(r)/2 + 1):
        for s in combinations(r, i):
            yield s, tuple(t for t in r if t not in s)

def cut(num):
    for s, t in cut_helper(num):
        yield s + (0,), t + (num - 1,)
        yield s + (num - 1,), t + (0,)

def get_paths(gr):
    num = len(gr.nodes())
    adj = build_adjacency(gr)
    paths1 = find_all_paths(adj, 0, num - 1)
    print('Paths: ')
    for p in paths1:
        print(p)
    paths2 = []
    for p in paths1:
        it1 = iter(p); it2 = iter(p[1:])
        paths2.append([e for e in izip(it1, it2)])
    return paths2

def get_min_path_weights(gr, paths):
    min_path_weights = set(min(gr.edge_weight(e) for e in p) for p in paths)
    min_path_weights = list(min_path_weights)
    print('Path min weights:', min_path_weights)
    return min_path_weights

def get_minmax_cuts(gr, val):
    num = len(gr.nodes())
    minmax_cuts = []
    max_cut_weights = set()
    cuts = list(cut(num))
    #print('Cuts:', cuts)
    for s, t in cuts:
        csetw = []
        for a, b in product(s, t):
            if a < b:
                e = a, b
            else:
                e = b, a
            if e in gr.edges():
                csetw.append(gr.edge_weight(e))
        if max(csetw) == val:
            minmax_cuts.append([s,t])
        max_cut_weights.add(max(csetw))
    max_cut_weights = list(max_cut_weights)
    print('Cut max weights:', max_cut_weights)
    print('MinMax cuts')
    for mm in minmax_cuts:
        print(mm)
    return max_cut_weights

def mark_edge(gr, paths, mval):
    for p in paths:
        em, vm = None, sys.maxint
        for e in p:
            v = gr.edge_weight(e)
            if v < vm:
                em, vm = e, v
        if vm == mval:
            gr.add_edge_attribute(em, ('penwidth','3'))

def display_graph(gr):
    dot = write(gr, weighted=True)
    args = ['/usr/bin/dot', '-Tpng', '-ograph.png']
    p = sp.Popen(args, stdin=sp.PIPE, stdout=None)
    p.communicate(dot)
    sp.call(['/usr/bin/eog', 'graph.png'])

def main():
    num_nodes = 5
    num_edges = int(2 * num_nodes)
    max_weight = 100
    gr = generate(num_nodes, num_edges, weight_range=(1,max_weight))

    gr.add_node_attribute(0, ('color','green'))
    gr.add_node_attribute(num_nodes - 1, ('color','red'))

    paths = get_paths(gr)

    maxP = max(get_min_path_weights(gr, paths))
    mark_edge(gr, paths, maxP)

    get_minmax_cuts(gr, maxP)

    sys.stdout.flush()

    display_graph(gr)

main()
