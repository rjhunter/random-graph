#!/usr/bin/env python

from __future__ import print_function
import sys
from itertools import combinations, product, izip
import subprocess as sp
import pygraph as pg
from pygraph.algorithms.generators import generate
from pygraph.readwrite.dot import write


def find_all_paths(adj, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    if not adj.has_key(start):
        return []
    paths = []
    for node in adj[start]:
        if node not in path:
            newpaths = find_all_paths(adj, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths

def build_adjacency(gr):
    adj = {}
    for node in gr.nodes():
        adj[node] = gr.neighbors(node)
    return adj

def cut_helper(num):
    r = set(xrange(1,num-1))
    for i in xrange(len(r)/2 + 1):
        for s in combinations(r, i):
            yield s, tuple(t for t in r if t not in s)

def cut(num):
    for s, t in cut_helper(num):
        yield s + (0,), t + (num - 1,)
        yield s + (num - 1,), t + (0,)

def get_paths(gr):
    num = len(gr.nodes())
    adj = build_adjacency(gr)
    paths1 = find_all_paths(adj, 0, num - 1)
    print('Paths: ')
    for p in paths1:
        print(p)
    paths2 = []
    for p in paths1:
        it1 = iter(p); it2 = iter(p[1:])
        paths2.append([e for e in izip(it1, it2)])
    return paths2

def get_min_path_weights(gr, paths):
    path_min_edges = list(set(min(p, key=lambda e: gr.edge_weight(e)) for p in paths))
    path_min_weights = list(set(gr.edge_weight(e) for e in path_min_edges))
    print('Path min weights:', path_min_weights)
    return path_min_weights, path_min_edges

def get_minmax_cuts(gr, val):
    num = len(gr.nodes())
    minmax_cuts = []
    max_cut_edges = set()
    for s, t in cut(num):
        csetw = []
        for a, b in product(s, t):
            if a < b:
                e = a, b
            else:
                e = b, a
            if e in gr.edges():
                csetw.append((e, gr.edge_weight(e)))
        if max(csetw, key=lambda e: e[1])[1] == val:
            minmax_cuts.append([s,t])
        max_cut_edges.add(max(csetw, key=lambda e: e[1]))
    print('Cut max weights:', [e[1] for e in max_cut_edges])
    print('MinMax cuts')
    for mm in minmax_cuts:
        print(mm)
    return zip(*max_cut_edges)

def mark_edge(gr, edge, style):
    attrs = { 'bold' : ('penwidth','3'),
              'dotted': ('style','dotted'),
              'blue' : ('color', 'blue'),
              'yellow' : ('color', 'yellow'),
              }
    gr.add_edge_attribute(edge, attrs[style])


def display_graph(gr):
    dot = write(gr, weighted=True)
    args = ['/usr/bin/dot', '-Tpng', '-ograph.png']
    p = sp.Popen(args, stdin=sp.PIPE, stdout=None)
    p.communicate(dot)
    sp.call(['/usr/bin/eog', 'graph.png'])

def main():
    num_nodes = 12
    num_edges = int(2 * num_nodes)
    max_weight = 100
    gr = generate(num_nodes, num_edges, weight_range=(1,max_weight))

    gr.add_node_attribute(0, ('color','green'))
    gr.add_node_attribute(num_nodes - 1, ('color','red'))

    paths = get_paths(gr)
    path_min_w, path_min_e = get_min_path_weights(gr, paths)

    max_path_min_w = max(path_min_w)
    max_path_min_e = max(path_min_e, key=lambda e: gr.edge_weight(e))

    mark_edge(gr, max_path_min_e, 'bold')
    for e in path_min_e:
        if e != max_path_min_e:
            mark_edge(gr, e, 'yellow')

    cut_max_e, cut_max_w = get_minmax_cuts(gr, max_path_min_w)

    for e in cut_max_e:
        if e != max_path_min_e:
            mark_edge(gr, e, 'blue')

    sys.stdout.flush()

    display_graph(gr)

main()
